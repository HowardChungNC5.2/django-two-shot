"""
URL configuration for expenses project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# import admin from django.contrib
# import path, include func from django.urls
# import redirect function from django.shortcuts

from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


# function that redirects to name="home" of urlpattern
# for receipts urls.py receipts list view (receipts/views.py)
def redirect_home(request):
    return redirect("home")


urlpatterns = [
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    # call to redirect_home function with path function
    path("", redirect_home, name="redirect_home"),
    path("accounts/", include("accounts.urls")),
]
