# import models function from django.db
# import settings function from django.conf
from django.db import models
from django.conf import settings

##### REMINDER: MAKE MIGRATIONS FOR MODELS AFTER: #####
##### CREATE • MODIFY • DELETE #####


# Create your models here.
# ExpenseCategory model is a value that we can apply to receipts like "gas" or "entertainment".
class ExpenseCategory(models.Model):
    # a name property that contains characters with a maximum length of 50 characters
    name = models.CharField(max_length=50)
    # an owner property that is a foreign key to the User  model with:
    owner = models.ForeignKey(
        # foreign key to User model
        settings.AUTH_USER_MODEL,
        # a related name of "categories"
        related_name="categories",
        # a cascade deletion relation
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


# Account model is the way that we paid for it, such as with a specific credit card or a bank account.
class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        # foreign key to User model
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


# Receipt model is the primary thing that this application keeps track of for accounting purposes.
class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(decimal_places=3, max_digits=10)
    tax = models.DecimalField(decimal_places=3, max_digits=10)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        # foreign key to User model
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        # foreign key to "ExpenseCategory" model
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        # foreign key to "Account" model
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
