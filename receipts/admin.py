# import admin from django.contrib
# import all registered models with admin from receipts.models
from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt


# @decorator that registers model with Django admin site
# to customize how model's data is displayed and managed in admin UI
@admin.register(ExpenseCategory)
# define custom admin class inherits from admin.ModelAdmin
# customize behavior and appearance of admin UI
class ExpenseCategoryAdmin(admin.ModelAdmin):
    # specifies fields displayed as columns in list view of admin UI
    list_display = ("name", "owner")


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        # ForeignKey is also field columns in db
        "purchaser",
        # ForeignKey is also field columns in db
        "category",
        # ForeignKey is also field columns in db
        "account",
    )
