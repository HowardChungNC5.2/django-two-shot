from django.shortcuts import render, redirect
from accounts.forms import LoginForm, SignupForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User


# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = LoginForm()

    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


# Feature 10 - Signup
def user_signup(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("home")
            else:
                # .add_error('<field_name>', '<error_message>')
                form.add_error("password", "the passwords do not match")
    else:
        form = SignupForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)


# handle showing singup form
# handle signup form submission

# create new user for HTTP POST using create_user method
# new user account includes username and password submitted via form
# if account is created
# redirect to list view of projects
# if password and password_confirmation do not match
# user should not be created
# and raise error that reads "the passwords do not match"
# show login form for HTTP GET
