# import path function from django.urls
# import LoginForm from accounts.forms
from django.urls import path
from accounts.views import user_login, user_logout, user_signup

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    # (url path in web browser, view function invoked, unique identifier for URL pattern in Django project)
    path("signup/", user_signup, name="signup"),
]
