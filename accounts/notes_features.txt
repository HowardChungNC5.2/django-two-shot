<!-- FEATURE 7: LOGINVIEW -->

# def login(request)
1. In Django, request object in a view function
refers to an HTTP request object
containing information about incoming request
from client browser to server to:

    • Access details about the request
    • Interact with the client's input and context.
    • Provide wide range of information and functionalities, including:

    • HTTP Method:
        • retrieving data (GET)
        • submitting data (POST)

    • Form Data:
        • For POST requests:
            -> access the data submitted via form fields

    • User Authentication:
        • determine if user is authenticated
        • get details about authenticated user

2. Specifications No.1-No.3
from django.shortcuts import render, redirect
from accounts.forms import LoginForm
from django.contrib.auth import authenticate, login


# HTTP request object containing information
# about incoming request from client browser
def user_login(request):
    # check if request is POST
    if request.method == "POST":
        # create LoginForm instance with associated submitted data
        form = LoginForm(request.POST)
        # if form data is valid with LoginForm fields
        if form.is_valid():
            # get cleaned data from LoginForm field values
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # import authenticate function from django.contrib.auth
            # check if request obj, username, password
            # match an existing user
            user = authenticate(request, username=username, password=password)
            # if user is a valid user object
            if user is not None:
                # import login function from django.auth.contrib
                # login func sets session data
                # to indicate user is authenticated
                login(request, user)
                # import redirect func from django.shortcuts
                # redirect to receipts list view
                # return user to receipts/urls.py path name="home"
                return redirect("home")
        # otherwise client request is GET data
        else:
            # create instance of blank LoginForm
            form = LoginForm()
        # wrap form in context dict to pass to template for rendering
        context = {
            "form": form,
        }
        return render(request, "accounts/login.html", context)

<!-- FEATURE 8: FILTERING RECEIPTS -->

1. Protect list view for Receipt model so only logged in users are given access

2. Change query set of view function to filter Receipt objects where purchaser equals logged in user

<!-- FEATURE 9: LOGOUT -->

1. from django.contrib.auth import logout
• create a function that logs a person out then redirects them to the URL path registration named "login"
• import logout function from django.contrib.auth

2. from accounts.views import user_logout
• register the view function in urlpatterns

<!-- FEATURE 10: LOGOUT -->

    href = "https://docs.djangoproject.com/en/4.0/topics/auth/default/#creating-users"

    1. >>> from django.contrib.auth.models import User
    >>> user = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')

    # At this point, user is a User object that has already been saved
    # to the database. You can continue to change its attributes
    # if you want to change other fields.
    >>> user.last_name = 'Lennon'
    >>> user.save()

    2. .add_error() method is a function provided by the django.forms.forms.Form class for adding errors to a specific form field. It allows you to attach error messages to individual form fields, which can then be displayed to the user when the form is rendered with errors.

    3.  Path Function Explained:

        path(
            url path in web browser,
            view function invoked,
            unique identifier for URL pattern in Django project
        )

        path("signup/", user_signup, name="signup"),

    4.  {% url %} Template Tag Explained:

        {% url %} template tag is used to reverse a URL by its name. In this case, it's trying to reverse the URL with the name "logout." The name "logout" should correspond to a URL pattern defined in your project's urls.py file.

        <a href="{% url 'logout' %}">Logout</a>
